__author__ = 'Joe Paxton'

import Logger
import socket
import errno
from datetime import datetime, timedelta 
from random import randint 
from random import randrange 
import random, time, struct, sys, os 
import multiprocessing 
from multiprocessing import Process

#Create Logger object so we can view potential errors that may occur
cwd = os.path.dirname(os.path.realpath(__file__))
log_path = "Client_Logs"
full_path = cwd + "//" + log_path

if not os.path.exists(full_path):
    os.mkdir(full_path)

logger_object = Logger.Logger(loggerName=os.path.basename(__file__)+".log", logPath=full_path)

class TestReports(object):
    def __init__(self, host, port):
        self.soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = host
        self.port = port
	self.soc.connect((self.host,self.port))
        self.suiteIDFromServer = ''
        self.msg_list=[]; self.curr_list=[];self.rec_list=[]

    def send_test_suites(self, msg):
        try:
            format_msg = self.format_send(msg)
            self.soc.sendall(format_msg)
            data = self.format_rcv()
            logger_object.info("TestSuiteID received from Server: " + str(data))
            if data.startswith('TestSuiteID'):
                keyword, self.suiteIDFromServer = data.split(',')
                logger_object.info("TestSuiteID from Server " + str(self.suiteIDFromServer))
        except socket.error, e:
            if e[0] == errno.EPIPE or e[0] == 32:
                self.recover_connection(msg)

    def send_test_cases(self, msg, recoverFlag=False, tcID=None):
        # Store incoming message into list before sending to Server
        self.curr_list.append(msg)
        logger_object.info("Current List: " + str(self.curr_list))

        # Normal flow of sending messages to Server
        if recoverFlag is False and tcID is None:
            format_msg = self.format_send(msg)
            try:
                self.soc.sendall(format_msg)
            except socket.error, e:
                if e[0] == errno.EPIPE or e[0] == 32:
                    self.recover_connection(msg)
            self.msg_list.append(msg) # Store incoming messages into list after sending to Server
            self.rec_list.append(msg) # This list will never be deleted
            logger_object.info("Message List: " + str(self.msg_list))

            # When going from Recovery back to Normal flow - messages are out of synch
            if len(self.msg_list) < len(self.curr_list):
                # Send the message within the list that has the correct messages
                format_msg = self.format_send(self.msg_list[-1])
                try:
                    self.soc.sendall(format_msg)
                except socket.error, e:
                    if e[0] == errno.EPIPE or e[0] == 32:
                        self.recover_connection(msg)
                finally:
                    # Delete both lists to avoid sending the same message over and over again
                    del self.curr_list[:]
                    del self.msg_list[:]

        # Recovery flow of sending messages to Server            
        else:
            try:
                tcID = int(tcID)
                lostTC = str(self.rec_list[tcID]) # Use this list because the others might be deleted
                tcMsg = "TestCases Recovery" + "," + str(lostTC) + "," + str(tcID)
                format_msg = self.format_send(tcMsg)
                logger_object.info("Message list: " + str(self.rec_list))
                self.soc.sendall(format_msg)
                logger_object.info("Lost TestCase data: " + str(format_msg))
            except socket.error, e:
                if e[0] == errno.EPIPE or e[0] == 32:
                    self.recover_connection(msg)

    def send_pdt(self, pdtMsg):
        format_msg = self.format_send(pdtMsg)
        try:
            self.soc.sendall(format_msg)
        except socet.error, e:
            if e[0] == errno.EPIPE or e[0] == 32:
                self.recover_pdt(pdtMsg)

    def recover_pdt(self, pdtMsg):
        while True:
            try:
                self.soc = socket.socket()
                self.soc.connect((self.host,self.port))
                logger_object.info("Re-connected to Server!")
                break
            except:
                logger_object.exception("Could not connect to Server")
        self.send_pdt(pdtMsg)

    def close_socket(self):
        close = self.format_send("Close")
        self.soc.sendall(close)
        logger_object.info("Closing socket connection...")
        data = self.format_rcv()
        logger_object.info("Server sent: " + str(data))
        self.soc.shutdown(2)
        self.soc.close()
        os._exit(0)

    def recover_connection(self, msg):
        # Attempt to reconnect - take out inf. loop to do every 'x' times every 'x' minutes
        while True:
            try:
                self.soc = socket.socket()
                self.soc.connect((self.host,self.port))
                logger_object.info("Re-connected to Server!")
                break
            except socket.error, e:
                logger_object.exception("Unable to re-connect to Server...")
        
        # Sending Disconnected keyword along with the last TestSuiteID it received from Server
        time.sleep(10)
        disconnectStr = "TestSuites Disconnected," + str(self.suiteIDFromServer)
        format_msg = self.format_send(disconnectStr)
        self.soc.sendall(format_msg)

        data = self.format_rcv()
        if data.startswith('TestCaseID'):
            keyword, tcID_fromServer = data.split(',')
        logger_object.info("TestCaseID from Server " + str(tcID_fromServer))

        # Recover from last TestCase in list
        recoverFlag=True
        self.send_test_cases(msg, recoverFlag, tcID_fromServer)
            
    def format_send(self, msg):
        msg_length = str(len(msg))
        while len(msg_length) < 4:
            msg_length = "0" + msg_length
        msg = msg_length + msg
        logger_object.info("Message sent to Server - inside format_send(msg): " + str(msg))
        return msg

    def format_rcv(self):
        data = ""
        size = int(self.soc.recv(4))
        while len(data) < size:
            message = self.soc.recv(size - len(data))
            data += message
        logger_object.info("Data received from Server - inside format_rcv(): " + str(data))
        return data
    
    def run_processes(self):
        try:
            logger_object.info("Starting Clients")
            self.connecting_to_server()
        except:
            logger_object.exception("Error while trying to connect to Server...")
        finally:
            logger_object.info("Killing sub-processes")
            logger_object.info("Shutting off processes")
            
def main():
    print("Check " + str(log_path) + " for logs once Client is finished sending all data...")
    
if __name__ == '__main__':
    main()

