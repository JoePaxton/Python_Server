__author__ = 'Joe Paxton'

import Logger 				    #in house module
import multiprocessing			    #used for multiple Client connections
from multiprocessing import Process	    #used to instantiate a processing object for Client
import MySQLdb as mdb			    #used for the connection from Server to MySQL database
import socket			    	    #used for connecting the Server and Client via socket
import struct, sys, os, time		    #multiple helper modules
import DB_CREDENTIALS as DB		    #in house module for database credential
from datetime import datetime, timedelta    #used for the run times and insert times

#Database Credentials
dbhost = DB.HOST
dbuser = DB.USER
dbpswd = DB.PSWD
dbname = DB.NAME

cwd = os.path.dirname(os.path.realpath(__file__))
log_path = "Server_Logs"
full_path = cwd + "//" + log_path

if not os.path.exists(full_path):
    os.mkdir(full_path)

logger_object = Logger.Logger(loggerName=os.path.basename(__file__)+".log", logPath=full_path)


class ServingData(object):
    """
    Listens to Client connections and handles TCP messages that will be sent 
    and inserts the message into the appropriate tables in the database.
    Handles multiple Clients connecting and sending TCP messages concurrently using 
    the multiprocessing module.
    When Server drops, it recovers lost TCP messages that Client was trying to send.
    """
    def __init__(self, host, port):
        self.host = host; self.port = port
        self.testCaseNum = 0; self.totalRunTime = timedelta(0, (0)) 
        self.passNo = 0; self.failNo = 0; self.skipNo = 0; self.executed = 0;

    def listen_to_clients(self):
        """
        Listens to Clients that are ready for connection and then calls an independent
        process for each Client connection and calls the get_message_from_clients()
        function to properly handle the sent data
        """
        self.soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.soc.bind((self.host,self.port))
        self.soc.listen(1) #how many connections can we listen to at one time?
        logger_object.info("Waiting for Client's connection...")

        while True:
            time.sleep(0.001)
            self.client_connection, self.client_address = self.soc.accept()
            logger_object.info("Received connnection from: " + str(self.client_address[0]))

            #Starting the background processes for each Client
            proc = Process(target=self.get_messages_from_clients)
            proc.daemon = True
            proc.start()
            logger_object.info("***" + str(proc) + " has started***")
            
    def get_messages_from_clients(self):
        """
        Process that goes through the Client's data that was sent via TCP and inserts each
        message into the PiHawk Database
        """
	self.database_connection = mdb.connect(dbhost, dbuser, dbpswd, dbname)
        self.cur = self.database_connection.cursor()

        if self.database_connection:
            logger_object.info("Successfully connected to " + str(dbname) + "...")
        else:
            logger_object.exception("Failed to connect to " + str(dbname) + "...")

        if not self.client_connection:
            logger_object.exception("Failed to connect to Client: " + str(self.client_address[0]) + "...")
        
        #Get SuiteID for TestSuite
        sql = "SELECT MAX(TestSuiteID) FROM TestSuites;"
        self.get_suiteID = self.fetch_sql_statement(sql)

        while True:
            time.sleep(0.001)
            data = ""
            # Calculates size of message and call 'recv' until you get full message
            try:
                size = int(self.client_connection.recv(4))
            except:
                continue
            logger_object.info("Size of message: " + str(size))

            while len(data) < size:
                if data is None:
                    logger_object.exception("Did not receive anything from the Client...")
                    self.client_connection.send("Please send data to Server")
                
                message = self.client_connection.recv(size - len(data))
                if not message:
                    logger_object.exception("Client needs to send data")
                    self.client_connection.send("Client needs to send data")
                    break
                data += message
                
                #TODO: data = data.lower() and lower() the checks...
                logger_object.info("Received this from the Client: " + str(data))

                #Client's TCP message starts with 'PDT' - insert PDT
                if data.startswith('PDT'):
                    self.insert_pdt(data)
                #Client's TCP message starts with 'TestSuites'
                elif data.startswith('TestSuites'):
                    #Client's connection dropped - Server will send TestCaseNum to Client
                    if data.startswith('TestSuites Disconnected'):
                        self.recover_transmission(data)
                    else:
                        #Client's connection has not dropped - insert TestSuites
                        self.insert_testsuites(data)
            
                #Client's TCP message starts with 'TestCases' - inserts TestCases
                elif data.startswith('TestCases'):
                    if data.startswith('TestCases Recovery'):
                        #Client's connection dropped - Server is receiving next TestCase msg where it left off
                        self.insert_recovered_testcases(data)
                    else:
                        #Client's connect has not dropped - insert TestCases
                        self.insert_testcases(data)
            
                #Client's TCP message starts with 'Close' - close socket
                elif data.startswith('Close'):
                    if data.startswith('Close PDT'):
                        self.close_pdt_socket()
                    else:
                        self.close_socket()
                    break

                #Client's TCP message was neither of the keywords above
                elif not data.startswith('PDT') or not data.startswith('TestCases') \
                     or not data.startswith('TestSuites') or not data.startswith('Close'):
                    self.data_error(data)
            #Break out of nested while loop
            else:
                continue
            break
        
        self.cur.close()
        self.database_connection.commit()
        
    def insert_pdt(self, data):
        """
        Takes Client's TCP message and inserts the data into the PDT table

        @param: data - client's tcp message
        """
        dataStart, user, version, office, scheduled = data.split(',')
        scheduled = int(scheduled)
        sched = 0
        if scheduled > 0:
            sched = 1
        logger_object.info("PDT message received: " + str(user) + "," + str(version) +\
                            "," + str(office) + "," + str(sched))
        insertTime = datetime.now()
        
        insert_into_sql = "INSERT INTO AutoUpgrades VALUES (%s,%s,%s,%s,%s);"
        try:
            column_values = (user, version, insertTime, office, int(sched))
            self.insert_into_table(insert_into_sql, column_values)
            logger_object.info("Inserted PDT: " + str(column_values))
        except:
            logger_object.exception("Could not log: " + str(column_values))

    def insert_testsuites(self, data):
        """
        Takes Client's TCP message and inserts the data into the TestSuites table

        @param: data - client's tcp message
        """
        #Split TCP message from Client to put into individual variables
        dataStart, self.suiteName, fileName, brand, build, toExecute = data.split(',')

        logger_object.info("TestSuites received: " + str(dataStart) + "," +  str(self.suiteName) +\
                "," + str(fileName) + "," + str(brand) + "," + str(build) + "," + str(toExecute))

        insertTime = datetime.now()
        logFile = "log" #Needs to be FTP
        ifPublished = '0'
        validation = "validated"

        #Insert parsed message into TestSuites table - NULL for suiteID to auto increment
        insert_into_sql = "INSERT INTO TestSuites VALUES (NULL,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"
        column_values = (self.suiteName, int(self.passNo), int(self.failNo),\
                int(self.skipNo), int(self.executed), fileName, insertTime, brand, build, int(ifPublished),\
                str(self.client_address[0]), self.totalRunTime, logFile, validation, toExecute)
        try:
            self.insert_into_table(insert_into_sql, column_values)
            logger_object.info("Inserted TestSuites values: " + str(column_values))
        except:
            logger_object.exception("Could not insert the following TestSuite data: " + str(column_values))

        #Get SuiteID for TestSuite
        sql = "SELECT MAX(TestSuiteID) FROM TestSuites;"
        self.get_suiteID = self.fetch_sql_statement(sql)

        #Send TestSuiteID to Client
        try:
            suiteIDStr = "TestSuiteID, " + str(self.get_suiteID)
            suiteID = self.format_send(suiteIDStr)
            self.client_connection.sendall(suiteID)
        except socket.error:
            logger_object.exception("Error - not able to send TestSuiteID to Client...")

        #Automation Site is in Testing mode
        autoupgrade_sql = "UPDATE AutomationSites SET Testing = %s WHERE IP = '%s';" %(1, self.client_address[0])
        self.execute_sql(autoupgrade_sql)
        #Not sure why this did not work in one line
        update_time = "UPDATE AutomationSites SET LastTestStart = '%s';" %(insertTime.replace(microsecond=0))
        self.execute_sql(update_time)

    def insert_testcases(self, data, tcID=None):
        """
        Takes Client's TCP message and inserts the data into the TestCases table

        @param: data - client's tcp message
        """
        #Recovery mode
        if tcID is not None:
            tcRecovery, tcKeyword, testName, testDesc, status, tabs, verifs, runTime, executionTime, tcID = data.split(',')
            sql = "SELECT MAX(Pass) FROM TestSuites WHERE TestSuiteID = " + str(self.get_suiteID) + ";"
            self.passNo = self.fetch_sql_statement(sql)

            sql = "SELECT MAX(Fail) FROM TestSuites WHERE TestSuiteID = " + str(self.get_suiteID) + ";"
            self.failNo = self.fetch_sql_statement(sql)

            sql = "SELECT MAX(Skip) FROM TestSuites WHERE TestSuiteID = " + str(self.get_suiteID) + ";"
            self.skipNo = self.fetch_sql_statement(sql)

            sql = "SELECT MAX(Executed) FROM TestSuites WHERE TestSuiteID = " + str(self.get_suiteID) + ";"
            self.executed = self.fetch_sql_statement(sql)
            
            testComments = "Recovered from dropped connection" #just to see in phpMyAdmin
            
            sql = "SELECT TestSuite FROM TestSuites WHERE TestSuiteID = " + str(self.get_suiteID) + ";"
            self.suiteName = self.fetch_sql_statement(sql)

            #This is the tcID sent from the Client to increment
            tcID = int(tcID)
            self.testCaseNum = int(tcID) + 1

            #Logging recovery mode data
            logger_object.info("RECOVERY MODE - Received TestCase data from Client: " +\
                    str(self.suiteName) + "," + str(testName) + "," + str(testDesc) +\
                    "," + str(status) + "," + str(tabs) + "," + str(verifs) + "," +\
                    str(runTime) + "," + str(executionTime) )

        #Normal mode
        else:
            dataStart, testName, testDesc, status, tabs, verifs, runTime, executionTime = data.split(',')
            testComments = ""
            self.testCaseNum += 1

        logger_object.info("Received TestCase data from Client: " +  str(self.suiteName) +\
                "," + str(testName) + "," + str(testDesc) + "," + str(status) + "," + str(tabs) + "," +\
                str(verifs) + "," + str(runTime) + "," + str(executionTime) )

        #Adding up pass, fail, skips from TestCases
        status = status.lower()
        if status == 'pass':
            self.passNo += 1
        elif status == 'fail':
            self.failNo += 1
        elif status == 'skip':
            self.skipNo += 1
        else:
            logger_object.exception("Status did not have 'Pass', 'Fail', or 'Skip' from Client...")
            self.client_connection.send("Status did not have 'Pass', 'Fail', or 'Skip' from Client...")
        self.executed = self.passNo + self.failNo

        #Insert Client's message into TestCases table
        insert_into_sql = "INSERT INTO TestCases VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"
        column_names = (self.testCaseNum, self.get_suiteID, self.suiteName, testName, testDesc, status, int(tabs),\
                int(verifs), runTime, testComments, executionTime)
        try:
            self.insert_into_table(insert_into_sql, column_names)
            logger_object.info("Inserting TestCases data: " + str(column_names))
        except:
            logger_object.exception("Could not insert TestCase data: " + str(column_names))

        alter_table = "UPDATE TestSuites SET Pass = " + str(self.passNo) + ", Fail = " +\
                str(self.failNo) + ", Skip = " + str(self.skipNo) + ", Executed = " +\
                str(self.executed) + " WHERE TestSuiteID = " + str(self.get_suiteID) + ";"
        self.execute_sql(alter_table)

        #Get all RunTimes from each TestCase
        sql = "SELECT TotalRunTime from TestSuites WHERE TestSuiteID = " + str(self.get_suiteID) + ";"
        total = self.fetch_sql_statement(sql)
        
        logger_object.info("TotalRunTime (before): " + str(total))

        tn_sql = "SELECT MAX(TestNumber) FROM TestCases WHERE TestSuiteID = " + str(self.get_suiteID) + ";"
        self.tn = self.fetch_sql_statement(tn_sql)
        
        #Total up all RunTimes from each TestCase to put in TotalRunTime
        runtime_sql = "SELECT RunTime FROM TestCases WHERE TestSuiteID = " + str(self.get_suiteID) +\
                " AND TestNumber = " + str(self.tn) + ";"
        rt = self.fetch_sql_statement(runtime_sql)
        
        logger_object.info("RunTime: " + str(rt))
        
        self.totalRunTime = total
        self.totalRunTime += rt

        logger_object.info("TotalRunTime (after): " + str(self.totalRunTime))

        alter_table = "UPDATE TestSuites SET TotalRunTime = '%s' WHERE TestSuiteID = %s;"\
                %(self.totalRunTime, self.get_suiteID)
        self.execute_sql(alter_table)

        logger_object.info("Updated TotalRunTime: " + str(self.totalRunTime))
        
        #Logging TestCase data
        logger_object.info("Inserting into TestCases:")
        logger_object.info("=========================")
        logger_object.info( (self.get_suiteID, self.testCaseNum, self.suiteName, testName, testDesc, status,\
                int(tabs), int(verifs), runTime,testComments, executionTime) )

    def recover_transmission(self, data):
        """
        Sequence control - Client will send the last SuiteID and the Server
        will send the last TestCaseID using the Client's SuiteID so the 
        Client can start sending the TestCases from the last TestCaseNum

        @param: data - TCP message to tell Server that it lost connection
        """
        #Selecting the last TestCaseNum from the TestSuiteID the Client sent
        dataStart, suiteIDFromClient = data.split(',')

        logger_object.info("After reconnection, received suiteID: " + str(suiteIDFromClient))

        sql = "SELECT MAX(TestNumber) FROM TestCases WHERE TestSuiteID = " + str(suiteIDFromClient) + ";"
        testIDForClient = self.fetch_sql_statement(sql)
        
        #Sending TestCaseNum to Client
        testIDString = "TestCaseID," + str(testIDForClient)
        testID = self.format_send(testIDString)
        self.client_connection.sendall(testID)
        logger_object.info("TestCaseID sending to Client: " + str(testIDString))

    def insert_recovered_testcases(self, data):
        """
        Client will send the last TestCase message it remembers before it lost connection
        and the Server will insert the TestCase message into the Database
        """
        tcRecovery, tcKeyword, testName, testDesc, status, tabs, verifs, runTime, executionTime, tcID = data.split(',')
        logger_object.info("TestCaseNum received from Client: " + str(tcID))
        self.insert_testcases(data, tcID)

    def close_socket(self):
        """
        Client sends a 'Close' message - so the Server closes the Client's socket connection and
        sets the Testing flag for the Client's IP back to False
        """
        if self.client_address[0] is not None:
            autoupgrade_sql = "UPDATE AutomationSites SET Testing = %s WHERE IP = '%s';" %(0, self.client_address[0])
            self.execute_sql(autoupgrade_sql)
        
        try:
            close = "Closing socket: Thanks for connecting " + str(self.client_address[0])
            closeMsg = self.format_send(close)
            self.client_connection.sendall(closeMsg)
            logger_object.info("Sent to Client: " + str(closeMsg))
        except:
            logger_object.exception("Could not send close socket message to Client")
        finally:
            self.client_connection.shutdown(2)
            self.client_connection.close()
            logger_object.info("Successfully closed socket connection")
        
    def close_pdt_socket(self):
	"""
	PDT sends a 'Close PDT' message - so the Server closes the Client's socket connection and sets the Testing
        flag for the Client's IP back to False
	"""
        if self.client_address[0] is not None:
            autoupgrade_sql = "UPDATE AutomationSites SET Testing = %s WHERE IP = '%s';" %(0, self.client_address[0])
            self.execute_sql(autoupgrade_sql)
        try:
            ticks = self.ticks(datetime.now())
            self.client_connection.send(str(ticks) + " Closing socket: Thanks for connecting " + str(self.client_address[0]))
            logger_object.info("Closing socket...")
        except:
            logger_object.exception("Could not send close message to Client")
        finally:
            self.client_connection.shutdown(2)
            self.client_connection.close()
            logger_object.info("Successfully closed socket connection to PDT")

    def fetch_sql_statement(self, sql):
        """
        Returns the value that your 'sql' statement is trying to query
        """
        self.cur.execute(sql)
        sql_return = self.cur.fetchone()[0]
        self.database_connection.commit()
        if type(sql_return) is int or type(sql_return) is float:
            return int(sql_return)
        elif type(sql_return) is str:
            return str(sql_return)
        else:
            return sql_return

    def insert_into_table(self, sql, column_values):
        """
        Inserts 'sql' query into a table using 'column_values'
        """
        self.cur.execute(sql, (column_values) )
        self.database_connection.commit()

    def execute_sql(self, sql):
        """
        Executes the 'sql' query
        """
        self.cur.execute(sql)
        self.database_connection.commit()

    def format_send(self, msg):
        """
        Formats the message for sending - pads the front of the message with 4 characters (size)
        """
        msg_length = str(len(msg))
        while len(msg_length) < 4:
            msg_length = "0" + msg_length
        msg = msg_length + msg
        logger_object.info("Inside format_send(msg) - Message that is being sent " + str(msg))
        return msg

    def format_rcv(self):
        """
        Formats and builds up the received message
        """
        data = ""
        size = int(self.soc.recv(4))
        while len(data) < size:
            message = self.soc.recv(size - len(data))
            data += message
        logger_object.info("Inside format_rcv() - Received this " + str(data))
        return data

    def ticks(self, date_time):
        t = datetime.utcfromtimestamp(0)
        return int((date_time - t).total_seconds())
    
    def testsuite_table_error(self):
        """
        This is a safety check to see if the 'TestSuites' table exists

        @param: database_connection - this is the connection to pihawk DB
        @param: client_connection   - this is the connection to client
        """
        self.cur.execute("SHOW TABLES LIKE 'TestSuites';")
        self.database_connection.commit()
        if self.cur.fetchone() is None:
            logger_object.exception("No table named 'TestSuites' in the " + dbname + " database")
            self.client_connection.send("No table named 'TestSuites' in the " + dbname + " database")

    def testcase_table_error(self):
        """
        This is a safety check to see if the 'TestCases' table exists

        @param: database_connection - this is the connection to pihawk DB
        @param: client_connection   - this is the connection to client
        """
        self.cur.execute("SHOW TABLES LIKE 'TestCases';")
        self.database_connection.commit()
        if self.cur.fetchone() is None:
            logger_object.exception("No table named 'TestCases' in the " + dbname + " database")
            self.client_connection.send("No table named 'TestCases' in the " + dbname + " database")


    def data_error(self, data):
        """
        This is a safety check to see if both tables exist and check
        if the Client's message starts with 'TestSuite' or 'TestCase'

        @param: database_connection - this is the connection to pihawk DB
        @param: client_connection   - this is the connection to client
        """
        self.testsuite_table_error()
        self.testcase_table_error()

        if not data.startswith('TestSuites') or not data.startswith('TestCases'):
            logger_object.exception("Make sure the message from Client starts with a keyword")  
            self.client_connection.send("Make sure the message from Client starts with a keyword")
           
    
    def run_processes(self):
        """
        This routine calls listen_to_clients() which starts independent processes for each Client
        that connects to the Server and concurrently processes data
        """
        logger_object.info("Listening to Clients")
        self.listen_to_clients()
        for proc in multiprocessing.active_children():
            proc.terminate()
            proc.join()
            logger_object.info("Killed all processes...")

def main():
    server = ServingData(host='xx.x.xx.x',port=xxxx) #security reasons
    server.run_processes()

if __name__ == '__main__':
    main()

