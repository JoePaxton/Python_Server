__author__ = 'Joe Paxton'

import client_tcp
from random import randint
from random import randrange
from datetime import datetime, timedelta
import random, time, struct, sys, os

client = client_tcp.TestReports(host='xx.xx.xx.xx',port=xxxx)

def send_test_reports():
    #Sending TestSuites message
    keyword = 'TestSuites'
    randSN = ['Network Regression', 'MWS Regression', 'Mini Regression', 'Stress Test']
    suiteName = random.choice(randSN)    
    fileName = sys.argv[0]
    randBrand = ['BP','CENEX','CENEX-Unbranded','CHEVRON','CHEVRON-Canada','CHEVRON-Unbranded','CITGO',\
                 'CITGO-Unbranded','CONCORD','CONOCOPHILLIPS','ESSI (IOL)','EXXON','HPS-Chicago','HPS-Dallas',\
                 'MARATHON','MARATHON-Unbranded','MOBIL','NBS','SHELL','SUNOCO','TEXACO','VALERO','WORLDPAY']
    brand = random.choice(randBrand)
    build = '11.02.21'
    toExecute = 100

    suiteMsg = keyword + "," + suiteName + "," + fileName + "," +  brand + "," + build + "," + str(toExecute)

    client.send_test_suites(suiteMsg)

    #Sending TestCase messages
    keyword2 = 'TestCases'
    i = 0
    tc_list = []
    while i < 50:
        i += 1
        testName = 'MoreStuff' + str(i) + '.py'
    	testDesc = 'Testing Speed Keys ' + str(i)
    	tabs = str(randint(0, 10))
    	verifs = str(randint(0, 100))
   			
    	executionTime = str(datetime.now())

    	rand = randint(0,2)
    	if rand == 2:
            status = 'PASS'
	if rand == 1:
            status = 'faIL'
	if rand == 0:
            status = 'sKiP'
				
	min_rand = randrange(1,5)
	sec_rand = randrange(5,59)
	runTime = timedelta(days=00, minutes=min_rand, seconds=sec_rand)

	tcMsg = keyword2 + "," + testName + "," + testDesc + "," + status + "," + tabs + "," +\
                verifs + "," + str(runTime) + "," + executionTime
	
        time.sleep(1)
        #tc_list.append(tcMsg)
        #print("Last element: " + str(tc_list[-1]))
	client.send_test_cases(tcMsg)

    #Sending Close message
    client.close_socket()

def send_pdt():
    keyword = 'PDT'
    user = 'joe'
    version = '11.02.21'
    office = 'GSO'
    scheduledRand = [0,1]
    scheduled = random.choice(scheduledRand)

    pdtMsg = keyword + ',' + user + ',' + version + ',' + office + ',' + str(scheduled)
    client.send_pdt(pdtMsg)

    close = "Close PDT"
    closeMsg = client.format_send(close)
    time.sleep(0.05)
    client.soc.sendall(closeMsg)

def main():
    send_test_reports()
    #send_pdt()

if __name__ == '__main__':
    main()


